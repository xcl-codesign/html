# Change Log

All notable changes to this project will be documented in this file.

- Format based on Keep A Change Log
- This project adheres to Semantic Versioning.


## [unreleased] - 2019-Apr-7.

### Added

- upload ui prototype
- new changelog

### Changed

- made Commit Entry summary public
- made Change Log revise public

### Deprecated

- index

### Removed

- Removed index

### Fixed

- index html

### Security

- nope
